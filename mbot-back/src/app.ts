import { Board, Proximity, ProximityData } from "johnny-five";
import { Server } from "socket.io";
import { MoveRobot} from "./MoveRobot";

const board = new Board();


const io = new Server({
  cors: {
    origin: '*'
  }
});

io.on('connection', (socket) => {

  const moveRobot: MoveRobot = new MoveRobot;

  board.on('ready', () => {
    console.log('connected');

    var proximity = new Proximity({
      freq: 1000,
      controller: "HCSR04",
      pin: 10
    });
  
    proximity.on("data", function(data : ProximityData) {
      console.log(data.cm);
      
      if(data.cm < 5) {
        console.log('stop');
        moveRobot.stop;
      }
    });

    socket.on('up', moveRobot.up);
    
    socket.on('left', moveRobot.left)
    socket.on('right', moveRobot.right)
    socket.on('down', moveRobot.down)
    socket.on('stop', moveRobot.stop)

  })
})

io.listen(8000);