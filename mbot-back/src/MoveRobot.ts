import { Board, Motor } from 'johnny-five';

export class MoveRobot {

  private motorLeft: Motor;
  private motorRight: Motor;

  constructor() {
    this.motorLeft = new Motor({pins: {pwm: 6, dir: 7}});
    this.motorRight = new Motor({pins: {pwm: 5, dir: 4}});
  }


  up() {
    console.log('up');
    this.move(100, this.motorLeft, this.motorRight, 'up');
}
left() {



  this.move(100, this.motorLeft, this.motorRight, 'left');

}
right() {


  this.move(100, this.motorLeft, this.motorRight, 'right');

}
down() {


  this.move(100, this.motorLeft, this.motorRight, 'down');

}

stop() {


  this.move(0, this.motorLeft, this.motorRight, 'stop');

}

private  move(speed: number, motorLeft: Motor, motorRight: Motor, key:string) {
  if (key) {
    switch (key) {
      case "up":
                motorLeft.reverse(speed);
                motorRight.forward(speed);
        break;
      case "down":
                motorRight.reverse(speed);
                motorLeft.forward(speed);
        break;
      case "left":
                motorLeft.forward(speed);
                motorRight.forward(speed);
        break;
      case "right":
                motorRight.reverse(speed);
                motorLeft.reverse(speed);
        break;
      case "stop":
        console.log('stopsopsoposp');
        
                motorLeft.stop();
                motorRight.stop();
        break;
    }
  }
}
  }









  



