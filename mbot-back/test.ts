// Uses proximity sensor on the front to tell the distance from the object
// Assumes prox sensor is plugged into port 2 - if not change the pin to use
// the pin for appropriate port and the second pin within it.

import { ProximityData } from "johnny-five";

var five = require("johnny-five");
var board = new five.Board();

board.on("ready", function() {
console.log('test');

  var proximity = new five.Proximity({
    freq: 1000,
    controller: "HCSR04",
    pin: 10
  });

  proximity.on("data", function(data : ProximityData) {
    console.log("inches: ", data.in);
    console.log("cm: ", data.cm);
  });
});